# Use a base image with Java (e.g., OpenJDK)
FROM openjdk:11-jre-slim

# Create and set the working directory
WORKDIR /app

# Copy the JAR file
COPY target/demo-0.0.1-SNAPSHOT.jar /app.jar

# Copy the application.properties file
COPY target/classes/application.properties /app/resources/

# Expose port 8082
EXPOSE 8082

# Set the entry point
ENTRYPOINT ["java", "-jar", "/app.jar"]

